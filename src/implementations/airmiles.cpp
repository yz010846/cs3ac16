#include "airmiles.hpp"
#include "utility.hpp"

#import <map>
#import <functional>

Airmiles::Airmiles(ListFlights &lf) {
    // If a precomputed lastflights file is supplied, then compute the airmiles.
    this->listFlights = lf;
    computeAirmiles();
}

void Airmiles::proc(std::vector<FullRecord> &r, std::vector<Airport> &a) {
    // Process from parameters.
    this->listFlights.proc(r, a);
    computeAirmiles();
}

void Airmiles::computeAirmiles() {
    // Iterate through each unique flight and compute its airmiles. Then for
    // every passenger on the flight increment their airmile counter. Then sort
    // to find the highest airmile flyer.
    std::map<std::string, double> airmilesMap;

    for (FullRecord flight : listFlights.flights) {
        double miles = utility::latLongToNm(
            flight.from->latitude, flight.from->longitude,
            flight.to->latitude, flight.to->longitude);
        
        for (std::string passenger : listFlights.passengers[flight.flight]) {
            airmilesMap[passenger] += miles;
        }
    }

    Comparator compFunctor =
        [](std::pair<std::string, double> a, std::pair<std::string, double> b){
            return a.second > b.second;
        };

    airmiles = std::set<std::pair<std::string, double>, Comparator>(
        airmilesMap.begin(), 
        airmilesMap.end(), compFunctor);
}