#include "count_airports.hpp"

#include <algorithm>
#include <iostream>


CountAirports::CountAirports() {}

void CountAirports::proc(std::vector<FullRecord> &r, std::vector<Airport> &a) {
    // Build a set of all the airport codes that exist, then build a list of all
    // the airports that are departed from and increment their number of
    // departures. Finally, compute the set difference between all the airport
    // codes and the airport codes that were departed from to compute the full
    // list of airports that had no departures.
    std::set<std::string> airportCodes;
    for (Airport &airport : a) {
        airportCodes.insert(airport.code);
    }

    std::set<std::string> flightIds;
    std::set<std::string> departedAirports;
    for (FullRecord &record : r) {
        if (flightIds.insert(record.flight).second) {
            ++flights[record.from->code];
            departedAirports.insert(record.from->code);
        }
    }

    std::set_difference(airportCodes.begin(), airportCodes.end(), 
        departedAirports.begin(), departedAirports.end(),
        std::inserter(undepartedAirports, undepartedAirports.begin()));

}


