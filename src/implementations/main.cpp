#include "signal.hpp"
#include "utility.hpp"
#include "airport.hpp"
#include "journey.hpp"
#include "full_record.hpp"
#include "count_airports.hpp"
#include "list_flights.hpp"
#include "airmiles.hpp"

#include <string>
#include <set>
#include <algorithm>
#include <time.h>

int main(int argc, char* argv[]) {
    // Terminate program if not properly supplied with data
    if (argc < 3) {
        signal::fail("Please supply both the airport, then journey data.");
    }

    // Process the airport file
    std::string airportFile(argv[1]);
    signal::notice("Loading Airport File: " + airportFile);
    std::string airportFileText = utility::removeWindowsLineEndings(
        utility::getFileContent(airportFile));
    std::istringstream airportFileTextStream(airportFileText);
    std::string line;
    int status;
    std::vector<Airport> airports;
    while (std::getline(airportFileTextStream, line)) {
        Airport airport(line, status);
        switch (status) {
            case 0:
                break;
            case 1:
                signal::warn("Discarding Airport Record '" + line + 
                    "', malformed.");
                continue;
            case 2:
                signal::warn("Discarding Airport Record '" + line + 
                    "', malformed number.");
                continue;
        }
        airports.push_back(airport);
    }
    signal::notice("Found " + std::to_string(airports.size()) + 
        " airport records.");

    // Process the journey file
    std::string journeyFile(argv[2]);
    signal::notice("Loading journey File: " + journeyFile);
    std::string journeyFileText = utility::removeWindowsLineEndings(
        utility::getFileContent(journeyFile));
    std::istringstream journeyFileTextStream(journeyFileText);
    std::vector<Journey> journeys;
    status = 0;
    while (std::getline(journeyFileTextStream, line)) {
        Journey journey(line, status);
        switch (status) {
            case 0:
                break;
            case 1:
                signal::warn("Discarding Journey Record '" + line +
                    "', malformed.");
                continue;
            case 2:
                signal::warn("Discarding Journey Record '" + line +
                    "', malformed number.");
                continue;
        }
        journeys.push_back(journey);
    }
    signal::notice("Found " + std::to_string(journeys.size()) +
        " journey records.");

    // Convert Airport Codes to Airport Objects.
    signal::notice("Unifying Journey Records and Airports");
    std::vector<FullRecord> records;
    for (Journey &journey : journeys) {
        FullRecord fr = FullRecord(journey, airports, status);
        switch (status) {
            case 1:
                signal::warn("Discarding Journey " + journey.passenger + "/" +
                    journey.flight + " due to unknown departure airport: " 
                    + journey.from);
                continue;
            case 2:
                signal::warn("Discarding Journey " + journey.passenger + "/" +
                    journey.flight + " due to unknown arrival airport: " 
                    + journey.to);
                continue;
        }
        records.push_back(fr);
    }
    signal::notice("Unified " + std::to_string(records.size()) + " journyes");
    std::stringstream output;

    // Objective 1
    signal::notice("OBJECTIVE 1 : Computing Airport Usage");
    CountAirports ca;
    ca.proc(records, airports);
    output << "Airport and number of departing flights recorded."
        << std::endl;
    for (auto &departure : ca.flights) {
        output << departure.first << ": " << departure.second << std::endl;
    }
    output << "The following airports have no departures." << std::endl;
    for (auto undepartedAirport : ca.undepartedAirports) {
        output << undepartedAirport << std::endl;
    }
    signal::out(output.str());
    output.str("");
    output.clear();

    // Objective 2
    signal::notice("OBJECTIVE 2 : Listing Flights and Passengers");
    ListFlights lf;
    lf.proc(records, airports);
    for (auto &flight : lf.flights) {
        time_t arrivalTime = flight.departureTime + (flight.flightTime * 60);
        output << "Flight " << flight.flight << " from " << flight.from->code 
            << " to " << flight.to->code << std::endl;
        char* procTime;
        procTime = ctime(&flight.departureTime);
        output << "Departed: " << procTime;
        procTime = ctime(&arrivalTime);
        output << "Arrived: " << procTime
            << "With passengers:" << std::endl;
        for (auto &passenger : lf.passengers[flight.flight]) {
            output << '\t' << passenger << std::endl;
        }
    }
    signal::out(output.str());
    output.str("");
    output.clear();

    // Objective 3, uses data from objective 2
    signal::notice(
        "OBJECTIVE 3 : Computing the number of passengers on each flight.");
    for (auto &flight : lf.flights) {
        output << "Flight " << flight.flight << " had " << std::to_string(
            lf.passengers[flight.flight].size()) << " passengers." << std::endl;
    }
    signal::out(output.str());
    output.str("");
    output.clear();

    // Objective 4, uses data from objective 2
    signal::notice(
        "OBJECTIVE 4 : Computing the airmiles of each passenger.");
    Airmiles am(lf);
    for (auto &airmiles : am.airmiles) {
        output << "Passenger " << airmiles.first << " has " << airmiles.second
            << " airmiles." << std::endl;
    }
    signal::out(output.str());
}