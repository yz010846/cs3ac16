#include "utility.hpp"

#include <cmath>

std::string utility::getFileContent(const std::string &filename) {
    std::ifstream in(filename);
    return std::string(
        (std::istreambuf_iterator<char>(in)), 
        std::istreambuf_iterator<char>());
}

std::string utility::removeWindowsLineEndings(std::string text) {
    std::string output;
    for (char &c : text) {
        if (c != '\r') {
            output += c;
        }
    }
    return output;
}

double utility::latLongToNm(double lat, double lon, double lat2, double lon2) {
    double x = 69.1 * (lat2 - lat);
    double y = 53.0 * (lon2 - lon);
    return sqrt((x * x) + (y * y));
}