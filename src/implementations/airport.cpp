#include "airport.hpp"

#include <iostream>

// Regular expression built according to given format, but including "/" and " "
// in the list of allowable characters for airport names so as not to exclude
// Dallas, New York, etc.
std::regex Airport::regex(
    R"(^([A-Z/ ]{3,20}),)"
    R"(([A-Z]{3}),)"
    R"(([+-]?[0-9]+\.[0-9]+),)"
    R"(([+-]?[0-9]+\.[0-9]+))");

Airport::Airport(std::string n, std::string c, double lat, double lon):
    name(n), code(c), latitude(lat), longitude(lon) {}

Airport::Airport(const std::string &line, int &status) {
    // Match the regular expression then fill fields based on the capture 
    // groups.
    int recordCount = 0;
    status = 0;

    std::smatch matches;
    if (!std::regex_search(line, matches, Airport::regex)) {
        status = 1;
        return;
    }

    std::smatch::iterator next = matches.begin();
    ++next;
    std::smatch::iterator end = matches.end();

    while (next != end) {
        //std::cout << next->str() << std::endl;
        switch (recordCount) {
            case 0:
                this->name = next->str();
                break;
            case 1:
                this->code = next->str();
                break;
            case 2:
                try {
                    this->latitude = std::stod(next->str());
                } catch (std::invalid_argument &e) {
                    status = 2;
                }
                break;
            case 3:
                try {
                    this->longitude = std::stod(next->str());
                } catch (std::invalid_argument &e) {
                    status = 2;
                }
                break;
        }
        ++next;
        ++recordCount;
    }
    return;
}