#include "list_flights.hpp"

#include <set>

ListFlights::ListFlights() {}

void ListFlights::proc(std::vector<FullRecord> &r, std::vector<Airport> &a) {
    // Explicitly mark `a` as unused, and store a unique instance for each
    // flight (including the first encountered passenger in the full record). 
    // Then store each encountered passenger against a map of their flight IDs.
    (void)a;

    std::set<std::string> uniqueFlights;

    for (FullRecord &record : r) {
        if (uniqueFlights.insert(record.flight).second) {
            flights.push_back(record);
        }
        passengers[record.flight].push_back(record.passenger);
    }
}