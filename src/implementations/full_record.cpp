#include "full_record.hpp"

#include <algorithm>

FullRecord::FullRecord(const Journey &journey, 
    const std::vector<Airport> &airports, int &status) {
    // Copy construct all of the fields other than the from and to field. For
    // both of those, look up the code in the airports vector paramater and
    // store a pointer to the airport object.

    status = 0;

    this->passenger = journey.passenger;
    this->flight = journey.flight;
    this->departureTime = journey.departureTime;
    this->flightTime = journey.flightTime;

    std::vector<Airport>::const_iterator from = std::find_if(
        airports.begin(), 
        airports.end(),
        [&journey](const Airport &a) {
            return (a.code == journey.from);
        });
    
    if (from == airports.end()) {
        status = 1;
        return;
    }

    std::vector<Airport>::const_iterator to = std::find_if(
        airports.begin(),
        airports.end(),
        [&journey](const Airport &a) {
            return (a.code == journey.to);
        });

    if (to == airports.end()) {
        status = 2;
        return;
    }

    this->from = &(*from);
    this->to = &(*to);
}