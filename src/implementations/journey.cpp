#include "journey.hpp"

#include <iostream>

std::regex Journey::regex(
    R"(^([A-Z]{3}[0-9]{4}[A-Z]{2}[0-9]),)"
    R"(([A-Z]{3}[0-9]{4}[A-Z]),)"
    R"(([A-Z]{3}),)"
    R"(([A-Z]{3}),)"
    R"(([0-9]{10}),)"
    R"(([0-9]{1,4}))");

Journey::Journey(std::string p, std::string fl, std::string fr, std::string t,
    time_t d, int ft):
    passenger(p), flight(fl), from(fr), to(t), departureTime(d), 
    flightTime(ft) {}

Journey::Journey(const std::string &line, int &status) {
    // Fill each field based on the regular expression capture groups.
    int recordCount = 0;
    status = 0;

    std::smatch matches;
    if (!std::regex_search(line, matches, Journey::regex)) {
        status = 1;
        return;
    }

    std::smatch::iterator next = matches.begin();
    ++next;
    std::smatch::iterator end = matches.end();

    while (next != end) {
        switch (recordCount) {
            case 0:
                this->passenger = next->str();
                break;
            case 1:
                this->flight = next->str();
                break;
            case 2:
                this->from = next->str();
                break;
            case 3:
                this->to = next->str();
                break;
            case 4:
                try {
                    this->departureTime = (time_t)std::stoi(next->str());
                } catch (std::invalid_argument &e) {
                    status = 2;
                }
                break;
            case 5:
                try {
                    this->flightTime = std::stoi(next->str());
                } catch (std::invalid_argument &e) {
                    status = 2;
                }
                break;
        }
        ++next;
        ++recordCount;
    }
    return;
}