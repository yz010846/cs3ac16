#include "signal.hpp"
#include "iostream"

#include <cstdlib>

void signal::_INTERNAL_OUTPUT(const std::string &message, const char *type) {
    signal::_INTERNAL_OUTPUT(message, type, ' ');
}

void signal::_INTERNAL_OUTPUT(const std::string &message, const char *type, const char sep) {
    std::cout << "[" << type << "]" << sep << message << std::endl << std::flush;
}

void signal::fail(const std::string &message) {
    signal::_INTERNAL_OUTPUT(message, "FAIL");
    exit(1);
}

void signal::notice(const std::string &message) {
    signal::_INTERNAL_OUTPUT(message, "NOTICE");
}

void signal::dump(const std::string &message) {
    signal::_INTERNAL_OUTPUT(message, "DUMP", '\n');
}

void signal::warn(const std::string &message) {
    signal::_INTERNAL_OUTPUT(message, "WARN");
}

void signal::out(const std::string &message) {
    signal::_INTERNAL_OUTPUT(message, "OUT", '\n');
}