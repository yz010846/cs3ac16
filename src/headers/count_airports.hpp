#pragma once
#ifndef COUNT_AIRPORTS_HPP_GUARD
#define COUNT_AIRPORTS_HPP_GUARD

#include "bulk_process.hpp"

#include <string>
#include <map>
#include <set>

/**
 * CountAirports is a processing class that implements the methods of
 * BulkProcess. It is used for objective 1 to count the number of different
 * times a plane leaves each airport.
 *
 * @author Lucille L. Blumire
 */
class CountAirports : public BulkProcess {
    public:
        /**
         * A map of the different number of times a plane leaves an airport.
         */
        std::map<std::string, int> flights;

        /**
         * A list of airports with no departures.
         */
        std::set<std::string> undepartedAirports;

        /**
         * Remove all duplicate instances of each flight ID, then count the
         * number of times each flight leaves.
         *
         * @param r The full records.
         * @param a The airports.
         */
        void proc(std::vector<FullRecord> &r, std::vector<Airport> &a) override;

        /**
         * The default constructor.
         */
        CountAirports();
};

#endif