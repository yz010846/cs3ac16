#pragma once
#ifndef LIST_FLIGHTS_HPP_GUARD
#define LIST_FLIGHTS_HPP_GUARD

#include "bulk_process.hpp"

#include <vector>
#include <map>

/**
 * ListFlights is a processing class that implements the methods of BulkProcess.
 * It is used in tasks 2, 3, and 4 in order to process the flights and their
 * inhabitants.
 *
 * @author Lucille L. Blumire
 */
class ListFlights : public BulkProcess {
    public:
        /**
         * A full list of unique flights.
         */
        std::vector<FullRecord> flights;

        /**
         * The set of passenger IDs for each flight.
         */
        std::map<std::string, std::vector<std::string>> passengers;

        /**
         * Create new records in the map for each flight and their pessangers.
         *
         * @param r The full records.
         * @param a The airports.
         */
        void proc(std::vector<FullRecord> &r, std::vector<Airport> &a) override;

        /**
         * The default constructor.
         */
        ListFlights();
};

#endif