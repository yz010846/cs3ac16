#pragma once
#ifndef JOURNEY_HPP_GUARD
#define JOURNEY_HPP_GUARD

#include <regex>
#include <string>

/**
 * This class provides a deserialization of the journey data provided by a
 * source CSV.
 *
 * @author Lucille L. Blumire
 */
class Journey {
    public:

        /**
         * Provides the regular expression form that is used to validate
         * the input CSV line for parsing.
         */
        static std::regex regex;

        /**
         * The list constructor builds the object from it's fields.
         *
         * @param p The passenger ID.
         * @param fl The flight ID.
         * @param fr The IATA/FAA code of the departure airport.
         * @param t The IATA/FAA code of the arrival airport.
         * @param d The departure time.
         * @param ft The total flight time in minutes.
         */
        Journey(std::string p, std::string fl, std::string fr, std::string t, 
            time_t d, int ft);

        /**
         * This constructor parses a CSV line containing the fields of the class
         * as records.
         *
         * @param line The CSV line.
         * @param status Modified to signal errors
         *        0 = OK
         *        1 = Malformed Entry
         *        2 = Malformed Number (likely to be 1 instead)
         */
        Journey(const std::string &line, int &status);

        /**
         * The ID of the passenger on a flight.
         */
        std::string passenger;

        /**
         * The ID of a flight.
         */
        std::string flight;

        /**
         * The IATA/FAA code of the starting airport.
         */
        std::string from;

        /**
         * The IATA/FAA code of the ending airport.
         */
        std::string to;

        /**
         * The time of departure.
         */
        time_t departureTime;

        /**
         * The total duration of the flight in minutes.
         */
        int flightTime; 
};

#endif