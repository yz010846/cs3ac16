#pragma once
#ifndef UTILITY_HPP_GUARD
#define UTILITY_HPP_GUARD

#include "bulk_process.hpp"
#include "full_record.hpp"

#include <string>
#include <fstream>
#include <streambuf>
#include <regex>
#include <vector>

/**
 * Provides a number of quick utility methods.
 *
 * @author Lucille L. Blumire
 */
namespace utility {
    /**
     * Loads the content of a file into a string.
     *
     * @param filename The name of the file to load.
     * @return The contents of the file.
     */
    std::string getFileContent(const std::string &filename);

    /**
     * Removes the \r characters from a string.
     *
     * @param text The string to strip of it's windows line endings.
     * @return The input text without any \r characters.
     */
    std::string removeWindowsLineEndings(std::string text);

    /**
     * Converts latitudes and longitudes into as-the-crow-flies nautical miles.
     * This is done using an approximation, but is accurate to within 12%.
     */
    double latLongToNm(double lat, double lon, double lat2, double lon2);
}

#endif