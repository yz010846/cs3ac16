#pragma once
#ifndef AIRMILES_HPP_GUARD
#define AIRMILES_HPP_GUARD

#include "list_flights.hpp"
#include <set>
#include <string>

/**
 * Type definition used for internal sorting.
 */
typedef std::function<bool(std::pair<std::string, double>, 
    std::pair<std::string, double>)> Comparator;


/**
 * Airmiles is a processing class that implements the methods of BulkProcess. It
 * is used for objective 4 to compute airmiles of each passenger based on each
 * flight. It recomputes that data from ListFlights and therefore can be passed
 * that on construction.
 *
 * @author Lucille L. Blumire
 */
class Airmiles : public BulkProcess {
    public:
        /**
         * The list of flights and their passengers.
         */
        ListFlights listFlights;

        /**
         * A map from the passenger ID to a total number of airmiles.
         */
        std::set<std::pair<std::string, double>, Comparator> airmiles;

        /**
         * Evaluate the airmiles for each passenger.
         *
         * @param r The full records of flights.
         * @param a The airports.
         */
        void proc(std::vector<FullRecord> &r, std::vector<Airport> &a) override;

        /**
         * Computers the airmiles based on the list of flights.
         */
        void computeAirmiles();

        /**
         * Constructs the airmiles and computes them based on an already
         * computed ListFlights object.
         * 
         * @param lf 
         */
        Airmiles(ListFlights &lf);
};

#endif