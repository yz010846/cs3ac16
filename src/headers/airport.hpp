#pragma once
#ifndef AIRPORT_HPP_GUARD
#define AIRPORT_HPP_GUARD

#include <regex>
#include <string>

/**
 * This class provides a deserialization of the airport data provided by a
 * source CSV.
 *
 * @author Lucille L. Blumire
 */
class Airport {
    public:

        /**
         * Provides the regular expression form that is used to validate
         * the input CSV line for parsing.
         */
        static std::regex regex;
        
        /**
         * The list constructor builds the object from it's fields.
         *
         * @param n The name of the airport.
         * @param c The IATA/FAA code of the airport.
         * @param lat The latitude of the airport.
         * @param lon The longitude of the airport.
         */
        Airport(std::string n, std::string c, double lat, double lon);
        
        /**
         * This constructor parses a CSV line containing the fields of the class
         * as records.
         *
         * @param line The CSV line constructing the fields.
         * @param status Modified to signal errors
         *        0 = OK
         *        1 = Malformed Entry
         *        2 = Malformed Number (likely to be 1 instead)
         */
        Airport(const std::string &line, int &status);

        /**
         * The full name of the airport.
         */
        std::string name;

        /**
         * The IATA/FAA code of the airport.
         */
        std::string code;

        /**
         * The latitude of the airport.
         */
        double latitude;

        /**
         * The longitude of the airport
         */
        double longitude; 
};

#endif