#pragma once
#ifndef SIGNAL_HPP_GUARD
#define SIGNAL_HPP_GUARD

#include <string>

/**
 * This namespace contains functions that provide command line interactivity.
 *
 * @author Lucille L. Blumire
 */
namespace signal {
    /**
     * Reports a critical error and terminate the execution of the programs.
     *
     * @param message The message output to the user.
     */
    void fail(const std::string &message);

    /**
     * Alerts the user of non critical information about the program execution.
     *
     * @param message The message output to the user.
     */
    void notice(const std::string &message);

    /**
     * Provides the user with a large block of multi line information, largely
     * for debugging purposes.
     *
     * @param message The message output to the user.
     */
    void dump(const std::string &message);

    /**
     * Warn the user of a non critical error, and how it is being handled
     * without program termination.
     *
     * @param message The message output to the user.
     */
    void warn(const std::string &message);

    /**
     * Signals an output section. Showing the results of computation.
     *
     * @param message The message output to the user.
     */
    void out(const std::string &message);

    /**
     * Internally used. Call other signals instead.
     */
    void _INTERNAL_OUTPUT(const std::string &message, const char *type, 
        const char sep);

    /**
     * Internally used. Call other signals instead.
     */
    void _INTERNAL_OUTPUT(const std::string &message, const char *type);
}

#endif