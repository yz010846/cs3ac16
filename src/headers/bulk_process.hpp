#pragma once
#ifndef BULK_PROCESS_HPP_GUARD
#define BULK_PROCESS_HPP_GUARD

#include "full_record.hpp"
#include "bulk_process.hpp"

/**
 * An abstract class providing the process method used by a number of other
 * features. In this program it is never dynamically dispatched upon though it 
 * could be were a more complex system of processing to be implemented than
 * simply performing each computation in series to demonstrate the needed
 * outputs.
 *
 * @author Lucille L. Blumire
 */
class BulkProcess {
        /**
         * Process and filter as needed the data.
         *
         * @param r The full list of records for pre-processing.
         * @param a The full list of the airports.
         */
        virtual void proc(std::vector<FullRecord> &r,
            std::vector<Airport> &a) = 0;
};

#endif