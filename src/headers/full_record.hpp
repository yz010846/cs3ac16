#pragma once
#ifndef FULL_RECORD_HPP_GUARD
#define FULL_RECORD_HPP_GUARD

#include "journey.hpp"
#include "airport.hpp"

#include <vector>
#include <string>

/**
 * This class provides a combination of journey data and airport data.
 *
 * @author Lucille L. Blumire
 */
class FullRecord {
    public:
        /**
         * Constructs a FullRecord from a journey, and a vector containing the
         * airports referenced in the journey.
         *
         * This class holds pointers to the airports, and thus it is important
         * that the airports vector is not invalidated after this.
         *
         * @param journey The journey being expanded into a full record.
         * @param airports The list of airports that might be referenced.
         * @param status Indicates any possible failure states:
         *        0 = OK
         *        1 = Could not find departure airport in airports.
         *        2 = Could not find arrival airport in airports.
         */
        FullRecord(const Journey &journey, const std::vector<Airport> &airports, 
            int &status);

        /**
         * The ID of the passenger on a flight.
         */
        std::string passenger;

        /**
         * The ID of a flight.
         */
        std::string flight;

        /**
         * The starting airport.
         */
        const Airport *from;

        /**
         * The ending airport.
         */
        const Airport *to;

        /**
         * The time of departure.
         */
        time_t departureTime;

        /**
         * The total duration of the flight in minutes.
         */
        int flightTime; 
};

#endif