CC = clang++
MKDIR_P = mkdir -p
TARGET = program

BUILDDIR = build
SRCDIR = src
OBJDIR = $(BUILDDIR)/obj
TARGETDIR = $(BUILDDIR)/target
HEADERDIR = $(SRCDIR)/headers
IMPLDIR = $(SRCDIR)/implementations
CFLAGS = -I$(HEADERDIR) -Wall -Wextra -Werror -g
ARGS = 

.PHONY: diretories

all: directories $(TARGETDIR)/$(TARGET)

directories: ${OBJDIR} ${TARGETDIR}

${OBJDIR}:
	${MKDIR_P} ${OBJDIR}

${TARGETDIR}:
	${MKDIR_P} ${TARGETDIR}

$(TARGETDIR)/$(TARGET): \
	$(OBJDIR)/main.o \
	$(OBJDIR)/signal.o \
	$(OBJDIR)/utility.o \
	$(OBJDIR)/airport.o \
	$(OBJDIR)/journey.o \
	$(OBJDIR)/full_record.o \
	$(OBJDIR)/count_airports.o \
	$(OBJDIR)/list_flights.o \
	$(OBJDIR)/airmiles.o
	
	$(CC) $(CFLAGS) -o $(TARGETDIR)/$(TARGET) $^

$(OBJDIR)/%.o: $(IMPLDIR)/%.cpp
	$(CC) $(CFLAGS) -o $@ -c $<

clean:
	rm -r $(OBJDIR)/* $(TARGETDIR)/*